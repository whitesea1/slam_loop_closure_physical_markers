#! usr/bin/env python
import rospy, roslib, cv2, message_filters
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from geometry_msgs.msg import PoseWithCovarianceStamped, Pose, PoseArray

class marker_detection():
    def __init__(self):
        try:
            image_topic = rospy.get_param("marker_detector/image_topic")
        except KeyError:
            rospy.signal_shutdown("marker_detector/image Rosparam not found")
        try:
            localization_topic = rospy.get_param("marker_detector/localization_topic")
        except KeyError:
            rospy.signal_shutdown("marker_detector/localization_topic Rosparam not found")

        self.sub1 = message_filters.Subscriber(image_topic, Image)
        self.sub2 = message_filters.Subscriber(localization_topic, PoseWithCovarianceStamped)
        self.ts = message_filters.ApproximateTimeSynchronizer([self.sub1, self.sub2], 10, 1)
        self.ts.registerCallback(self.callback)
        self.pub = rospy.Publisher('checkpoints', PoseArray, queue_size = 10)

        self.poses = PoseArray()
        time = rospy.get_rostime()
        poses.header.seq = 0
        poses.header.stamp.secs = time.secs
        poses.header.stamp.nsecs = time.nsecs

        self.pub.publish(self.poses)

    def callback(self, im, pose):
