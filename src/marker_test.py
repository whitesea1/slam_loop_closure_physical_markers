#! /usr/bin/env python
import rospy, roslib, cv2, message_filters
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class marker_detection():
    def __init__(self):
        try:
            image_topic = rospy.get_param("marker_detector/image_topic")
        except KeyError:
            rospy.signal_shutdown("marker_detector/image Rosparam not found")
        try:
            self.encoding = rospy.get_param("marker_detector/encoding")
        except KeyError:
            rospy.signal_shutdown("marker_detector/encoding Rosparam not found")
        try:
            self.marker_color = rospy.get_param("marker_detector/color")
        except KeyError:
            rospy.signal_shutdown("marker_detector/color Rosparam not found")
        self.sub1 = rospy.Subscriber(image_topic, Image, self.callback)
        self.bridge = CvBridge()

    def callback(self, im):
        cv_image = self.bridge.imgmsg_to_cv2(im, desired_encoding=self.encoding)
        filtered = self.filter(cv_image)
        circles = self.findCircles(filtered)
        if not(circles==None):
            for i in circles[0,:]:
                cv2.circle(filtered,(i[0],i[1]),i[2],(0,0,255),2)
                rospy.loginfo(str(i[2]))
        cv2.imshow('filtered_image', filtered)
        cv2.imshow('real_image', cv_image)
        cv2.waitKey(3)

    def filter(self,im):
        # standard color is orange
        hsv = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)
        boundaries = ([5, 50,50],[15,255,255])
        if self.marker_color == 'pink':
            boundaries = ([150, 100, 100], [165,255,255])
        elif self.marker_color == 'green':
            boundaries = ([55,75,75],[75,255,255])

        lower = np.array(boundaries[0], dtype = "uint8")
        upper = np.array(boundaries[1], dtype = "uint8")
        mask = cv2.inRange(hsv, lower, upper)
        output = cv2.bitwise_and(im,im, mask=mask)
        return output

    def findCircles(self,im):
        gray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
        circles = cv2.HoughCircles(gray,cv2.cv.CV_HOUGH_GRADIENT, 1, 10, param1=25,param2=13,minRadius=0,maxRadius=12)
        if not(circles == None):
            circles = np.uint16(np.around(circles))
            return circles
        else:
            return None

if __name__ == "__main__":
    rospy.init_node('marker_detector')
    marker_detector = marker_detection()
    rospy.spin()
