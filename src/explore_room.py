#!/usr/bin/env python
import rospy, roslib, cv2, message_filters, time, tf
from nav_msgs.msg import OccupancyGrid
from move_base_msgs.msg import MoveBaseActionResult
from geometry_msgs.msg import Pose, PoseStamped, PoseWithCovariance, PoseWithCovarianceStamped
from std_msgs.msg import Float64
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import numpy as np
from tf.transformations import quaternion_from_euler
from math import atan2, pi, sqrt
from nav_msgs.srv import GetMap
from time import sleep
from copy import deepcopy

class slam_explorer():
    def __init__(self):
        self.checkpoints = []
        self.threshold = 3.4011
        self.current_pose = Pose()
        self.listener = tf.TransformListener()
        self.bridge = CvBridge()
        self.encoding = 'bgr8'
        self.marker_color = 'green'

        # Set up move_base and map subscriber
        self.sub1 = rospy.Subscriber('/move_base/result', MoveBaseActionResult, self.callback1)

        # TODO: set up entropy and camera subscriber
        self.sub2 = rospy.Subscriber('/slam_gmapping/entropy', Float64, self.callback2)

        # self.sub3 = message_filters.Subscriber('/amcl_pose', PoseWithCovarianceStamped)
        # self.sub4 = message_filters.Subscriber('/head_camera/rgb/image_raw', Image)
        # self.ts = message_filters.ApproximateTimeSynchronizer([self.sub3,self.sub4], 10, 1)
        # self.ts.registerCallback(self.callback3)
        self.sub3 = rospy.Subscriber('/head_camera/rgb/image_raw', Image, self.callback3)
        # Set up publisher
        self.pub = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=10)

        rospy.wait_for_service('/dynamic_map')
        self.getMap = rospy.ServiceProxy('/dynamic_map', GetMap)
        self.failures = 0
        sleep(2)
        self.pub.publish(self.get_goal())

    def callback1(self, msg):
        done_status = [3,4,5]
        if msg.status.status in done_status:
            if msg.status.status in done_status[1:]:
                self.failures = self.failures + 1
                if self.failures > 5:
                    rospy.signal_shutdown("Done exploring")
            else:
                self.failures = 0

            self.pub.publish(self.get_goal())

    def callback2(self,entropy):
	rospy.loginfo("callback")
        if len(self.checkpoints) > 0 and entropy.data > self.threshold:
            rospy.loginfo("closing a loop")
            current = deepcopy(self.current_pose)
            mindist = float('inf')
            new = Pose()
            for point in self.checkpoints:
                if self.euclidian_dist(current, point) < mindist:
                    mindist = self.euclidian_dist(current, point)
                    new = deepcopy(point)
            goal = PoseStamped()
            goal.pose = new
            goal.header.stamp = rospy.Time.now()
            goal.header.frame_id = 'map'
            self.pub.publish(goal)



    # def callback3(self, pose, im):
    def callback3(self,im):
        try:
            (trans,rot) = self.listener.lookupTransform('map', 'base_link', rospy.Time(0))
            pose = Pose()
            pose.position.x = trans[0]
            pose.position.y = trans[1]
            pose.position.z = trans[2]
            pose.orientation.x = rot[0]
            pose.orientation.y = rot[1]
            pose.orientation.z = rot[2]
            pose.orientation.w = rot[3]

            self.current_pose = pose
            if len(self.checkpoints) == 0:
                self.checkpoints.append(deepcopy(pose))
            cv_image = self.bridge.imgmsg_to_cv2(im, desired_encoding=self.encoding)
            filtered = self.filter(cv_image)
            circles = self.findCircles(filtered)
            if not(circles==None):
                rospy.loginfo("found marker")
                self.checkpoints.append(deepcopy(pose))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            pass

    # Define helper functions
    def euclidian_dist(self,point1,point2):
        dx = point2.position.x - point1.position.x
        dy = point2.position.y - point1.position.y
        return sqrt(dx*dx + dy*dy)

    def get_goal(self):
        resp = self.getMap()
        grid = resp.map
        map_data = grid.data
        resolution = grid.info.resolution
        width = grid.info.width
        height = grid.info.height
        origin = grid.info.origin
        locations = []
        for i in range(width):
            for j in range(height):
                if map_data[j*width + i] == -1:
                    locations.append((i,j))
        if len(locations) == 0:
            rospy.signal_shutdown("Done exploring")
        ind = np.random.choice(range(len(locations)))
        new_goal = locations[ind]
        newx = new_goal[0] * resolution + origin.position.x
        newy = new_goal[1] * resolution + origin.position.y

        position = Pose()
        position.position.x = newx
        position.position.y = newy
        position.position.z = 0.0
        quat = quaternion_from_euler(0,0,np.random.uniform(-pi,pi))
        position.orientation.x = quat[0]
        position.orientation.y = quat[1]
        position.orientation.z = quat[2]
        position.orientation.w = quat[3]

        goal = PoseStamped()
        goal.pose = position
        goal.header.stamp = rospy.Time.now()
        goal.header.frame_id = "map"

        return goal

    def filter(self,im):
        # standard color is orange
        hsv = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)
        boundaries = ([5, 50,50],[15,255,255])
        if self.marker_color == 'pink':
            boundaries = ([140, 100, 100], [170,255,255])
        elif self.marker_color == 'green':
            boundaries = ([55,75,75],[75,255,255])

        lower = np.array(boundaries[0], dtype = "uint8")
        upper = np.array(boundaries[1], dtype = "uint8")
        mask = cv2.inRange(hsv, lower, upper)
        output = cv2.bitwise_and(im,im, mask=mask)
        return output

    def findCircles(self,im):
        gray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
        circles = cv2.HoughCircles(gray,cv2.cv.CV_HOUGH_GRADIENT, 1, 10, param1=25,param2=13,minRadius=0,maxRadius=12)
        if not(circles == None):
            circles = np.uint16(np.around(circles))
            return circles
        else:
            return None

if __name__ == "__main__":
    rospy.init_node('slam_explorer')
    explorer = slam_explorer()

    rospy.spin()
